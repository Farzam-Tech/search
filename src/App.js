import React, { Component , Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import Layout from './hoc/Layout/Layout';
import FullPost from './components/posts/Fullpost/Fullpost';

const Posts = React.lazy(() => import('./components/posts/Posts'))
class App extends Component {
  render () {
    return (
      <div>
        <Layout>
          <Switch>
            
        <Route path="/posts" exact render={(props) => (
              <Suspense fallback={<div>Loading...</div>}>
                <Posts {...props}/>
              </Suspense>
         )} />
            <Route path={'/restaurants/:id'} exact component={FullPost} />
          </Switch>
        </Layout>
      </div>
    );
  }
}

export default App;

