import * as actionTypes from './actionTypes';
import axios from '../../axios-posts'

export const setCities = (cities) =>{
    return{
        type: actionTypes.SET_CITIES,
        cities: cities
    }
}; 
export const fetchCitiesFailed = () =>{
   return {
       type: actionTypes.FETCH_CITIES_FAILED
   }
};
export const initCities = () =>{
    return dispatch =>{
       
        axios.get('/cities')
            .then( response => {
                dispatch(setCities(response.data.cities));
                // const cities = response.data.cities;
                // this.setState({cities : cities , loading:false})
            //    console.log(response)
                    }).catch(error => {
                     dispatch(fetchCitiesFailed())
                        // console.log(error);
                        // this.setState({error:true , loading:false});
                    });
    }
}