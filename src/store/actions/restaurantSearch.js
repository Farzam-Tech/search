import * as actionTypes from './actionTypes';
import axios from '../../axios-posts';


export const restaurantSearchSuccess = (restaurants) => {
    return{
        type: actionTypes.RESTAURANT_SEARCH_SUCCESS,
        restaurants : restaurants
    }

};
export const restaurantSearchFail = (error) =>{
    return{
        type: actionTypes.RESTAURANT_SEARCH_FAIL,
        error: error
    }
};
export const restaurantSearchStart = ()=>{
    return {
        type: actionTypes.RESTAURANT_SEARCH_START
    };
};
export const restaurantFound = (restaurants)=>{
    return{
        type: actionTypes.RESTAURANT_FOUND,
        restaurants : restaurants
    };
};
export const filterRestaurantsByDetail = (restaurants , searchinfo)=>{
    return{
        type: actionTypes.RESTAURANT_FILTER_BY_DETAIL,
        restaurants : restaurants, 
        searchinfo : searchinfo
    }
}
export const initFullPostStart = () =>{
    return {
        type: actionTypes.INIT_FULL_POST_START
    };
}
export const initFullPostSuccess = (loadedPost)=>{
    return{
        type: actionTypes.INIT_FULL_POST_SUCCESS,
        loadedPost: loadedPost
    }
}
export const initFullPostFail = (error)=>{
    return{
        type: actionTypes.INIT_FULL_POST_FAIL,
        error: error
    }
}
export const searchRestaurant = (search) => {
    
    return dispatch => {
        dispatch(restaurantSearchStart());
        axios.get(`/restaurants?city=${search.City.value}`)
        .then( response => {
            // console.log(response.data.restaurants);
            
            //   const responseData = response.data.restaurants;
              dispatch(restaurantSearchSuccess(response.data.restaurants));
              if(search.Detail.value){
                 dispatch(filterRestaurantsByDetail(response.data.restaurants , search.Detail.value));
              }
            //   console.log(search.Detail.value);
              dispatch(restaurantFound(response.data.restaurants));
              
                 
                }).catch(error => {
                  dispatch(restaurantSearchFail(error))
                });
              
}

    }
export const initFullPost = (id , loadedPost) =>{
    return dispatch=>{
        dispatch(initFullPostStart());
        if (id) {
            if ( !loadedPost || (loadedPost && loadedPost.name !== +id) ) {
            axios.get( `/restaurants?name=${id}`)
            .then( response => {
                dispatch(initFullPostSuccess(response.data.restaurants[0]));
                // this.setState( { loadedPost: response.data.restaurants[0], loading : false} );
            } ).catch(error => {
                // console.log(error);
                // this.setState({error:true , loading :false});
                dispatch(initFullPostFail(error))
            });
            }
        }
      
    }
}