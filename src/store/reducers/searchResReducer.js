import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';
// import { initFullPostSuccess, initFullPostFail, initFullPostStart } from '../actions/restaurantSearch';
const initialState = {
   restaurants: [],
   loadedPost: null,
   loading: false, 
   found: false,
   error: false
}
const restaurantSearchStart = (state, action) =>{
    return updateObject(state ,
         {  loading: true,
        error: false, found: false});
};
const restaurantFound = (state, action)=>{
    const res = state.restaurants;
    if(res.length > 0){
        return updateObject(state , { found: true});
    }
    
};
const restaurantSearchSuccess = (state, action)=>{
    
    return updateObject(state ,
         {restaurants: action.restaurants,
        loading: false});  
};
const restaurantSearchFailed = (state, action) =>{
    return updateObject(state , { error: true,
        loading: false});
};
const restaurantSearchByDetail = (state, action) =>{
    const restaurants = [...action.restaurants];
        let updatedRestaurants = restaurants.filter((res) => {
                      return(
                            res.name.toLowerCase().includes( action.searchinfo) ||
                            res.area.toLowerCase().includes( action.searchinfo ) ||
                            res.address.toLowerCase().includes( action.searchinfo)
                           ) 
                         
    })

return updateObject(state,
     {restaurants : updatedRestaurants}
     );
   
}
const initFullPostStart = (state, action) => {
    return updateObject(state , {  loading: true, error: false});
    
}
const initFullPostSuccess = (state, action)=>{
    return updateObject(state , 
        {loadedPost: action.loadedPost,
        loading: false}); 
}
const initFullPostFail= (state, action) => {
    return updateObject(state , { error: true,
        loading: false});
}
const searchReducer = (state = initialState, action) => {
switch (action.type){
    case actionTypes.RESTAURANT_SEARCH_START: return restaurantSearchStart(state, action);
    case actionTypes.RESTAURANT_FOUND: return restaurantFound(state, action);   
    case actionTypes.RESTAURANT_SEARCH_SUCCESS:  return restaurantSearchSuccess(state, action);
    case actionTypes.RESTAURANT_SEARCH_FAIL: return restaurantSearchFailed(state, action);
    case actionTypes.RESTAURANT_FILTER_BY_DETAIL: return restaurantSearchByDetail(state,action);
    case actionTypes.INIT_FULL_POST_START: return initFullPostStart(state,action);
    case actionTypes.INIT_FULL_POST_SUCCESS: return initFullPostSuccess(state, action);
    case actionTypes.INIT_FULL_POST_FAIL: return initFullPostFail(state,action);
    default: return state;
}
};
export default searchReducer;
