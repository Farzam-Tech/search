import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility'
const initialState = {
    cities: [],
    error: false
};
const setCities = (state , action)=>{
    return updateObject(state , {cities: action.cities,
        error: false});
};
const fetchCitiesFailed = ( state , action) =>{
    return updateObject(state , { error: true });
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.SET_CITIES: return setCities(state , action);
        case actionTypes.FETCH_CITIES_FAILED: return fetchCitiesFailed(state , action);
                default:
                    return state;
    }
    };
    
    export default reducer;