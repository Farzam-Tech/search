import React, { Component } from 'react';
import axios from '../../../axios-posts'
import Spiner from '../../UI/Spinner/Spinner';
import classes from './Fullpost.module.css'
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
class FullPost extends Component {

    componentDidMount () {
        
       this.props.onInitFullPost(this.props.match.params.id , this.props.loadedPost);
    }

  
    render () {
        
        let post = <p style={{ textAlign: 'center' }}>something went wrong!</p>;
        if ( this.props.match.params.id && !this.props.error) {
            post = <p style={{ textAlign: 'center' }}>Loading...!</p>;
            if (this.props.loading){
                post = <Spiner/>
            }
        }
        if ( this.props.loadedPost && !this.props.error ) {
            post = (
              
                <section className={classes.fullpost}>
                  <div className={classes.postdetails}>
                     <figure className={classes.postimage}><img src={ this.props.loadedPost.image} alt="pic"/></figure>

                  <div className={classes.highlights}>
                      <strong className={classes.items}>{this.props.loadedPost.name}</strong>
                      <p className={classes.items}><strong>Area : </strong>{this.props.loadedPost.area}</p>
                      <p className={classes.items }><strong>Price : </strong>{this.props.loadedPost.price}</p>
                      <p className={classes.items}><strong>Address : </strong>{this.props.loadedPost.address}</p>
                  </div>
                  </div>
                </section>
                 
          

            );
        }
        return post;
      
    }
    
}
const mapStateToProps = state => {
    return {
        loadedPost: state.searchRes.loadedPost,
        error: state.searchRes.error,
        loading: state.searchRes.loading
    };
}
const mapDispatchToProps = dispatch => {
    return {
       onInitFullPost : (id ,loadedPost)=> dispatch(actions.initFullPost(id ,loadedPost )),
      

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FullPost , axios);
