import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import axios from '../../axios-posts';
import Post from './Post/Post';
import SearchInputForm from '../SearchInputsForm/SearchInputForm'
import classes from './Posts.module.css';
import Spiner from '../UI/Spinner/Spinner'
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'
class cities extends Component {
    state = {
        selectedPostId: null,
    }

    componentDidMount () {
    this.props.onInitCities();
                  
                   
    }

      postSelectedHandler = (id) => {
        this.props.history.push( '/restaurants/' + id );
     
        
    }
    
    CitySearchHandler = (search)=>{
        this.props.onSearchRestaurants(search);
    }

    render () {
      
    let restaurants = <p style={{textAlign: 'center'}}>something went wrong!</p>;
        
        

        if (!this.props.error && this.props.restaurants) {
            restaurants = this.props.restaurants.map(res => {
                return <Post 
                    key={res.id} 
                    post={res}
                    clicked={() => this.postSelectedHandler(res.name)} />;
                
          
           
             
            })}
        
        if (this.props.loading){
            restaurants = <Spiner/>
        }
             
                return (
                    
                    <>
                    <SearchInputForm cities={this.props.cities} CitySearchHandler={this.CitySearchHandler}/>
                    <section className={classes.posts}>
                    {restaurants} 
                    </section>
                      </>
                   
                  
             );
         
        }


}
const mapStateToProps = state => {
    return {
        restaurants: state.searchRes.restaurants,
        cities: state.cities.cities,
        error: state.cities.error,
        loading: state.searchRes.loading,
        found: state.searchRes.found
    };
}

const mapDispatchToProps = dispatch => {
    return {
       onInitCities : ()=> dispatch(actions.initCities()),
       onSearchRestaurants: (searchData) => dispatch(actions.searchRestaurant(searchData))

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(cities , axios ));