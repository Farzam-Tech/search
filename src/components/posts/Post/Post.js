import React from 'react';
import classes from './Post.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBookmark } from '@fortawesome/free-solid-svg-icons'; 
const post = (props) => (
    
   <div className={classes.post} onClick={props.clicked}>
       <figure className={classes.postimage}><img src={props.post.image} alt={`${props.post.name}Pic`}/></figure>
       <div className={classes.postinfo}>
           <div className={classes.postinfoitems}>
         <h4>{props.post.name}</h4><FontAwesomeIcon icon={faBookmark} />
           </div>
           <div className={classes.postinfoitems}>
              <p> <strong>Area:</strong>  {props.post.area} , <strong>City:</strong> : {props.post.city}</p>
           </div>
           <div className={classes.postinfoitems}>
            <p><strong>Address:</strong> {props.post.address} , <strong>Price:</strong>{props.post.price} </p>
           </div>

       </div>
   </div>
           

);


export default post;